# JmxOnline Api

This is the Api part of the JMXOnline service

## Getting Started

### Prerequisites

* [Maven](https://maven.apache.org/) - Dependency Management

### Installing
open command prompt and cd to the project root

run
```
mvn spring-boot:run
```

## Built With

* [Spring](https://spring.io) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details