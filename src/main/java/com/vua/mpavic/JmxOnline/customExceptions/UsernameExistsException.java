package com.vua.mpavic.JmxOnline.customExceptions;

public class UsernameExistsException extends Exception {

    public UsernameExistsException(String s) {
        super(s);
    }
}
