package com.vua.mpavic.JmxOnline.customExceptions;

public class CannotConnectJmxException extends Exception {

    public CannotConnectJmxException(String s) {
        super(s);
    }
}
