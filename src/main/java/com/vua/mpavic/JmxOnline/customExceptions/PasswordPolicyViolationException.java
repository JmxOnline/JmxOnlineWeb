package com.vua.mpavic.JmxOnline.customExceptions;

public class PasswordPolicyViolationException extends  Exception {

    public PasswordPolicyViolationException(String message) {
        super(message);
    }
}
