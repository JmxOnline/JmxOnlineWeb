package com.vua.mpavic.JmxOnline.customExceptions;

public class AccessViolationException extends  Exception {

    public AccessViolationException(String s) {
        super(s);
    }
}
