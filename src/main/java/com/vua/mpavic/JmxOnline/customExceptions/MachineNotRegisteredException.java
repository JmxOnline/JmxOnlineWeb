package com.vua.mpavic.JmxOnline.customExceptions;

public class MachineNotRegisteredException extends Exception {


    public MachineNotRegisteredException(String s) {
        super(s);
    }
}
