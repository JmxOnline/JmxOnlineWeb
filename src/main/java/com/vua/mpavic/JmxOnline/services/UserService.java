package com.vua.mpavic.JmxOnline.services;


import com.vua.mpavic.JmxOnline.customExceptions.UsernameExistsException;
import com.vua.mpavic.JmxOnline.dao.model.AppUser;
import com.vua.mpavic.JmxOnline.dao.model.Role;
import com.vua.mpavic.JmxOnline.dao.repos.AppUserRepository;
import com.vua.mpavic.JmxOnline.dao.repos.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import org.hibernate.exception.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements  UserServices {
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private RoleRepository roleRepository;




    @Override
    public void registerNewUser(AppUser appUser) throws UsernameExistsException {
        appUser.setPassword(encoder.encode(appUser.getPassword()));
        appUser.setEnabled(1);

        List<Role> roles = new ArrayList<>();
         Role r = roleRepository.findRoleByRoleName("ROLE_USER");
        roles.add(r);
        appUser.setRoles(roles);
        try{
            appUserRepository.save(appUser);
        }catch(Exception ex){
            this.handleUsernameExists(ex);
        }


    }

    private void handleUsernameExists(Throwable ex) throws UsernameExistsException {
       if(ex instanceof  ConstraintViolationException){
           throw new UsernameExistsException("Username already exists");
       }
       if(ex == null){
           return;
       }
       handleUsernameExists(ex.getCause());
    }

    @Override
    public void addUpdateUser(AppUser user) {
        appUserRepository.save(user);
    }

    @Override
    public AppUser getUserByUsername(String username) {
        return appUserRepository.getByUsername(username);
    }


}
