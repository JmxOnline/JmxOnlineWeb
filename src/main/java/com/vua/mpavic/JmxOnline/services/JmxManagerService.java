package com.vua.mpavic.JmxOnline.services;

        import com.sun.management.OperatingSystemMXBean;
        import com.vua.mpavic.JmxOnline.controllers.utils.ResponseString;
        import com.vua.mpavic.JmxOnline.customExceptions.CannotConnectJmxException;
        import com.vua.mpavic.JmxOnline.customExceptions.MachineNotRegisteredException;
        import com.vua.mpavic.JmxOnline.dao.model.WorkloadType;

        import javax.xml.ws.Response;
        import java.io.IOException;
        import java.net.MalformedURLException;

public interface JmxManagerService {


    ResponseString createConnection(WorkloadType type, String apiKey) throws MachineNotRegisteredException, IOException, CannotConnectJmxException;

    boolean closeConnection(WorkloadType type, String apiKey);

    void pingConnection(String apiKey);


}
