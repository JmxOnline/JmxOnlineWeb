package com.vua.mpavic.JmxOnline.services;

import com.vua.mpavic.JmxOnline.customExceptions.UsernameExistsException;
import com.vua.mpavic.JmxOnline.dao.model.AppUser;

public interface UserServices   {
    void registerNewUser(AppUser appUser) throws UsernameExistsException;

    void addUpdateUser(AppUser user);
    AppUser getUserByUsername(String username);



}
