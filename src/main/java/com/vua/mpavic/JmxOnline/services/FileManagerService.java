package com.vua.mpavic.JmxOnline.services;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;

public interface FileManagerService {

    public void storeFile(byte[] file);
    File getfile(String name);
}
