package com.vua.mpavic.JmxOnline.services;

import com.sun.management.OperatingSystemMXBean;
import com.vua.mpavic.JmxOnline.controllers.utils.ResponseString;
import com.vua.mpavic.JmxOnline.customExceptions.CannotConnectJmxException;
import com.vua.mpavic.JmxOnline.customExceptions.MachineNotRegisteredException;
import com.vua.mpavic.JmxOnline.dao.jmxManagement.JmxManager;
import com.vua.mpavic.JmxOnline.dao.model.JmxConnection;
import com.vua.mpavic.JmxOnline.dao.model.ManagedMachine;
import com.vua.mpavic.JmxOnline.dao.model.SocketConnection;
import com.vua.mpavic.JmxOnline.dao.model.WorkloadType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.net.MalformedURLException;


@Service
public class JmxManagerSeriviceImpl implements JmxManagerService {

    @Autowired
    ManagedMachineService service;
    @Autowired
    SimpMessagingTemplate template;
    JmxManager manager = new JmxManager();

    @Override
    public ResponseString createConnection(WorkloadType type, String apikey) throws MachineNotRegisteredException, CannotConnectJmxException {
        try {
            ManagedMachine managedMachine = service.getByApiKey(apikey);

            if(managedMachine.getJmxUrl() == null){
                throw new MachineNotRegisteredException("Machine is not registered, please add the api key to your app and start the application");
            }
            SocketConnection connection = new SocketConnection();
            JmxConnection jmxconnection = new JmxConnection();
            jmxconnection.setJmxURl(managedMachine.getJmxUrl());

            JMXServiceURL url = new JMXServiceURL(managedMachine.getJmxUrl());
        JMXConnector connector = null;

            connector = JMXConnectorFactory.connect(url, null);

            jmxconnection.setConnector(connector);
            connection.setConnection(jmxconnection);
            connection.setApiKey(managedMachine.getApiKey());
            manager.addConnection(type,connection,template);
            return new ResponseString(managedMachine.getApiKey());
        } catch (IOException e) {
            throw  new CannotConnectJmxException("Cannot connect to machine, please check your ports and ensure the app is running");
        }
    }

    @Override
    public boolean closeConnection(WorkloadType type, String apiKey) {
        manager.removeConnection(type,apiKey);
        return  true;
    }

    @Override
    public void pingConnection(String apiKey){
        manager.pingConnection(apiKey);
    }
}
