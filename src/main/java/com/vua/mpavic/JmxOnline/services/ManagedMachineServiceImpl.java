package com.vua.mpavic.JmxOnline.services;

import com.vua.mpavic.JmxOnline.customExceptions.AccessViolationException;
import com.vua.mpavic.JmxOnline.dao.model.ManagedMachine;
import com.vua.mpavic.JmxOnline.dao.repos.ManagedMachineRepository;
import com.vua.mpavic.JmxOnline.services.utils.JmxKeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.List;

@Service
public class ManagedMachineServiceImpl implements ManagedMachineService {
@Autowired
    ManagedMachineRepository repository;


    @Override
    public ManagedMachine getByApiKey(String apiKey) {
        return  repository.findByApiKey(apiKey);
    }

    @Override
    public List<ManagedMachine> getByUsername(String username) {

        List<ManagedMachine> managedMachines = repository.findAllByUser_Username(username);

     return managedMachines;
    }

    @Override
    public String insertNewMachine(ManagedMachine managedMachine) {

        managedMachine.setApiKey(JmxKeyUtil.generateJMXKey());
      return repository.save(managedMachine).getApiKey();
    }

    @Override
    public void registerMachine(String jmxKey, String jmxUrl) {
        ManagedMachine m = repository.findByApiKey(jmxKey);
        m.setJmxUrl(jmxUrl);
        repository.save(m);
    }

    @Override
    public ManagedMachine getById(int id) {
        return repository.findManagedMachineById(id);
    }

    @Override
    public List<ManagedMachine> getDashboardByUser(String username) {
        return repository.findAllByUser_UsernameAndOnDashboard(username,true);
    }

}

