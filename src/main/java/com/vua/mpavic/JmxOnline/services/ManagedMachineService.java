package com.vua.mpavic.JmxOnline.services;

import com.vua.mpavic.JmxOnline.dao.model.ManagedMachine;

import java.util.List;

public interface ManagedMachineService {

    ManagedMachine getByApiKey(String apiKey);
    List<ManagedMachine> getByUsername(String username);
    String insertNewMachine(ManagedMachine managedMachine);
    void registerMachine(String jmxKey,String jmxUrl);
    ManagedMachine getById(int id);
    List<ManagedMachine>getDashboardByUser(String username);

}
