package com.vua.mpavic.JmxOnline.config.JWTSupport;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.vua.mpavic.JmxOnline.dao.model.ApiErrorResponce;
import com.vua.mpavic.JmxOnline.dao.model.AppUser;
import com.vua.mpavic.JmxOnline.services.UserServices;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class   JWTLoginFilter extends AbstractAuthenticationProcessingFilter {



    public JWTLoginFilter(String url, AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, BadCredentialsException {

        ObjectMapper objMap = new ObjectMapper();
        objMap.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE,false);
        AppUser cred = objMap.readValue(httpServletRequest.getInputStream(),AppUser.class);

        AuthenticationManager manager = getAuthenticationManager();
        Authentication auth;

            auth = manager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            cred.getUsername(),cred.getPassword(),Collections.emptyList()));
            return auth;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {


        UserServices userService;

        ServletContext servletContext = request.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        userService = webApplicationContext.getBean(UserServices.class);

        JwtTokenUtil.addAuthentication(request,response,authResult.getName(),userService);

        response.setStatus(200);
        response.getWriter().write("");
        response.getWriter().flush();
        response.getWriter().close();
        return;

    }
}
