package com.vua.mpavic.JmxOnline.config;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

public class UtilBase64Image {

    public static String encode(Resource resource){

        try(InputStream image= resource.getInputStream()){
            String base64Image = "";

            byte imageData[] = new byte[(int)resource.getFile().length()];

            image.read(imageData);

            base64Image = Base64.encode(imageData);
            return base64Image;

        }catch (IOException ex){
            return "Image not found";
        }
    }
}
