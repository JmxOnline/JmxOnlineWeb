package com.vua.mpavic.JmxOnline.controllers.utils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseString {
    private String response;


    public ResponseString(String response) {
        this.response = response;
    }

    public ResponseString() {
    }
}
