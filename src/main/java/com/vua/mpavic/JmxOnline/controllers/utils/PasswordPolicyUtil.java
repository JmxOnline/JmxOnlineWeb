package com.vua.mpavic.JmxOnline.controllers.utils;

import com.vua.mpavic.JmxOnline.customExceptions.PasswordPolicyViolationException;
import com.vua.mpavic.JmxOnline.dao.model.AppUser;

public class PasswordPolicyUtil {

    public static void checkPassword(AppUser appUser) throws PasswordPolicyViolationException {
        String password = appUser.getPassword();
        if(password == null){
            throw new PasswordPolicyViolationException("Password is empty");
        }
        if(password.length() < 5){
            throw new PasswordPolicyViolationException("Password is too short");
        }
    }
}
