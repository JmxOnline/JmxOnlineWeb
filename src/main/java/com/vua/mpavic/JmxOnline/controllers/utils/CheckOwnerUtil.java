package com.vua.mpavic.JmxOnline.controllers.utils;

import com.vua.mpavic.JmxOnline.customExceptions.AccessViolationException;

public class CheckOwnerUtil {


    public static void checkOwner(String usernameObject, String usernameToken) throws AccessViolationException {

        if(!usernameObject.equals(usernameToken)){
            throw new AccessViolationException("Not the owner");
        }
    }
}
