package com.vua.mpavic.JmxOnline.controllers;

import com.vua.mpavic.JmxOnline.controllers.utils.PasswordPolicyUtil;
import com.vua.mpavic.JmxOnline.controllers.utils.ResponseString;
import com.vua.mpavic.JmxOnline.customExceptions.PasswordPolicyViolationException;
import com.vua.mpavic.JmxOnline.customExceptions.UsernameExistsException;
import com.vua.mpavic.JmxOnline.dao.model.AppUser;
import com.vua.mpavic.JmxOnline.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserServices services;





    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public ResponseString successLogin() {
        return new ResponseString("Login successfull");
    }


    @PostMapping
    public ResponseString registerUser(@RequestBody AppUser appUser) throws PasswordPolicyViolationException, UsernameExistsException {

        PasswordPolicyUtil.checkPassword(appUser);

        services.registerNewUser(appUser);
        ResponseString response = new ResponseString();
        response.setResponse("new user inserted");
        return response;
    }

}
