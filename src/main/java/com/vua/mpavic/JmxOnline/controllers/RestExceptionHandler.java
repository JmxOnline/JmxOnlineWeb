package com.vua.mpavic.JmxOnline.controllers;

import com.vua.mpavic.JmxOnline.customExceptions.*;
import com.vua.mpavic.JmxOnline.dao.model.ApiErrorResponce;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@EnableWebMvc
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {


    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "Malformed JSON request";
        return ResponseEntity.badRequest().body(new ApiErrorResponce(HttpStatus.BAD_REQUEST,error,ex,HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler(PasswordPolicyViolationException.class)
    protected ResponseEntity<Object> handlePasswordViolationException(PasswordPolicyViolationException ex){
        ApiErrorResponce responce = new ApiErrorResponce(HttpStatus.BAD_REQUEST,HttpStatus.BAD_REQUEST.value());
        responce.setMessage("Password does not meet standards");
        responce.setDebugMessage(ex.getLocalizedMessage());
        return ResponseEntity.badRequest().body(responce);
    }



    @ExceptionHandler(AccessViolationException.class)
    protected ResponseEntity<Object> handleTokenExpired(AccessViolationException ex){
        ApiErrorResponce responce = new ApiErrorResponce(HttpStatus.UNAUTHORIZED,HttpStatus.UNAUTHORIZED.value());
        responce.setMessage("Access violation");
        responce.setDebugMessage(ex.getLocalizedMessage());
        return ResponseEntity.badRequest().body(responce);
    }

    @ExceptionHandler(MachineNotRegisteredException.class)
    protected ResponseEntity<Object> handleMachineNotRegistered(MachineNotRegisteredException ex){
        ApiErrorResponce responce = new ApiErrorResponce(HttpStatus.BAD_REQUEST,HttpStatus.BAD_REQUEST.value());
        responce.setMessage("Machine not registered");
        responce.setDebugMessage(ex.getLocalizedMessage());
        return ResponseEntity.badRequest().body(responce);
    }
    @ExceptionHandler(CannotConnectJmxException.class)
    protected ResponseEntity<Object> handleMachineNotRegistered(CannotConnectJmxException ex){
        ApiErrorResponce responce = new ApiErrorResponce(HttpStatus.BAD_REQUEST,HttpStatus.BAD_REQUEST.value());
        responce.setMessage("Cannot connect to machine, please check your ports and ensure the app is working");
        responce.setDebugMessage(ex.getLocalizedMessage());
        return ResponseEntity.badRequest().body(responce);
    }


    @ExceptionHandler(UsernameExistsException.class)
    protected ResponseEntity<Object> handleIUsernameExists(UsernameExistsException ex){
        ApiErrorResponce responce = new ApiErrorResponce(HttpStatus.BAD_REQUEST,HttpStatus.BAD_REQUEST.value());
        responce.setMessage("Username allready exists");
        responce.setDebugMessage(ex.getLocalizedMessage());
        return ResponseEntity.badRequest().body(responce);
    }



}
