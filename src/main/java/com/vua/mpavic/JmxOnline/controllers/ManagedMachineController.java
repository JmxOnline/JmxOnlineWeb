package com.vua.mpavic.JmxOnline.controllers;

import com.vua.mpavic.JmxOnline.config.JWTSupport.JwtTokenUtil;
import com.vua.mpavic.JmxOnline.controllers.utils.CheckOwnerUtil;
import com.vua.mpavic.JmxOnline.controllers.utils.ResponseString;
import com.vua.mpavic.JmxOnline.customExceptions.AccessViolationException;
import com.vua.mpavic.JmxOnline.dao.model.ManagedMachine;
import com.vua.mpavic.JmxOnline.dao.model.jmxModel.ClientRegistration;
import com.vua.mpavic.JmxOnline.services.ManagedMachineService;
import com.vua.mpavic.JmxOnline.services.UserService;
import org.hibernate.engine.spi.Managed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/managedMachines")
public class ManagedMachineController {


    @Autowired
    ManagedMachineService service;
    @Autowired
    UserService userService;
    @GetMapping
    public ResponseEntity<List<ManagedMachine>> getManagedMachinesByUser(HttpServletRequest request)  {

        String token = request.getHeader("Authorization");

        String username = JwtTokenUtil.unpackToken(token);
        List<ManagedMachine> machines = service.getByUsername(username);

        return ResponseEntity.ok(machines);
    }

    @GetMapping("/{machineID}")
    public ResponseEntity<ManagedMachine> getMachineById(@PathVariable int machineID,HttpServletRequest request) throws AccessViolationException {
        ManagedMachine machine = service.getById(machineID);
        String token = request.getHeader("Authorization");

        String username = JwtTokenUtil.unpackToken(token);

        CheckOwnerUtil.checkOwner(machine.getUser().getUsername(),username);

        return ResponseEntity.ok(machine);

    }


    @PostMapping()
    public ResponseEntity<ResponseString> createMachine(@RequestBody ManagedMachine managedMachine,HttpServletRequest request){
        ResponseString response  = new ResponseString();
        String token = request.getHeader("Authorization");

        String usernname = JwtTokenUtil.unpackToken(token);
        managedMachine.setUser(  userService.getUserByUsername(usernname));
        response.setResponse(service.insertNewMachine(managedMachine));

        
       return ResponseEntity.ok(response);
    }

    @CrossOrigin
    @PostMapping("/register")
    public ResponseEntity<Boolean> registerMachine(@RequestBody ClientRegistration managedMachine){
        service.registerMachine(managedMachine.getJmxKey(),managedMachine.getJmxUrl());

        return ResponseEntity.ok(true);
    }

    @CrossOrigin
    @GetMapping("/dashboard")
    public ResponseEntity<List<ManagedMachine>> getDashboardMachines(HttpServletRequest request){
        String token = request.getHeader("Authorization");

        String usernname = JwtTokenUtil.unpackToken(token);
        List<ManagedMachine> machines = service.getDashboardByUser(usernname);
        return ResponseEntity.ok(machines);
    }


}
