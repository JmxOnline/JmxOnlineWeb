package com.vua.mpavic.JmxOnline.controllers;

import com.vua.mpavic.JmxOnline.controllers.utils.ResponseString;
import com.vua.mpavic.JmxOnline.customExceptions.CannotConnectJmxException;
import com.vua.mpavic.JmxOnline.customExceptions.MachineNotRegisteredException;
import com.vua.mpavic.JmxOnline.dao.model.WorkloadType;
import com.vua.mpavic.JmxOnline.services.FileManagerService;
import com.vua.mpavic.JmxOnline.services.JmxManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/jmx")
public class JmxController {

    @Autowired
    JmxManagerService service;


    @Autowired
    FileManagerService fileService;

    // TODO: 7/12/2018 change this to handle with api key
    @RequestMapping(value = "/connections/{type}/{apiKey}",method = RequestMethod.GET)
    public ResponseEntity<ResponseString> createConnection(@PathVariable WorkloadType type, @PathVariable String apiKey) throws MachineNotRegisteredException, IOException, CannotConnectJmxException {
        return ResponseEntity.ok().body( service.createConnection(type,apiKey));
    }
    @RequestMapping(value = "/connections/{type}/{apiKey}",method = RequestMethod.DELETE)
    public boolean closeConnection(@PathVariable WorkloadType type,@PathVariable String apiKey){
        return service.closeConnection(type,apiKey);
    }

    @GetMapping(value = "/connections/ping/{apiKey}")
    public void pingConnection(@PathVariable String apiKey){
        service.pingConnection(apiKey);
    }


    @GetMapping(value = "/jmxSDK.jar")
    public void  downloadJar(HttpServletResponse response) throws IOException {
        File f = fileService.getfile("jmxSDK.jar");

        InputStream stream = new FileInputStream(f);

        response.setContentType("application/java-archive");
        org.apache.commons.io.IOUtils.copy(stream,response.getOutputStream());
        response.flushBuffer();

    }
}
