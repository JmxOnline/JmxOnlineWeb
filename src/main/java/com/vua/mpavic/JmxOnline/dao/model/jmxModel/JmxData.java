package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import javafx.application.Platform;

import java.lang.management.PlatformManagedObject;
import java.util.Optional;

public interface JmxData {

    public Optional<Object> getData();

    public String getUrl();
}
