package com.vua.mpavic.JmxOnline.dao.model;

import com.vua.mpavic.JmxOnline.dao.model.jmxModel.JmxData;
import com.vua.mpavic.JmxOnline.dao.model.jmxModel.JmxDataFactory;
import javafx.application.Platform;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import sun.security.acl.WorldGroupImpl;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.PlatformManagedObject;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class WorkloadImpl implements Workload {

    private static long SLEEP_TIME = 1000;
    SocketConnection connection;
    String apiKey;
    Thread selfReference;
    boolean paused;
    private int MAX_TIMEOUT = 1800;
    SimpMessagingTemplate template;
    WorkloadType type;
    private boolean on;
    JmxDataFactory factory = new JmxDataFactory();

    public void setType(WorkloadType type) {
        this.type = type;

        calculateSleepTime(type);
    }

    private void calculateSleepTime(WorkloadType type) {
        switch (type){
            case THREAD_DATA:
                SLEEP_TIME = 5000;
                break;
            default:
                SLEEP_TIME = 1000;
                break;
        }
    }

    public WorkloadImpl(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Override
    public void addConnection(SocketConnection connection) {
        this.connection = connection;
        apiKey = connection.getApiKey();
    }

    @Override
    public void setOff() {
        on = false;
    }

    @Override
    public void setOn() {
        on = true;
        if (selfReference == null) {
            selfReference = new Thread(this);
        }
        selfReference.setName(apiKey);
        selfReference.start();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void ping() {
        connection.resetCounter();
        //todo check if we need to pause the thread here
    }

    @Override
    public boolean isOn() {
        return on;
    }

    @Override
    public WorkloadType getType() {
        return type;
    }

    @Override
    public String getApiKey() {
        return apiKey;
    }

    @Override
    public void run() {

        while (on) {
            if (paused == false && connection != null) {
                try {
                    System.out.println(connection.getKeepAlive());
                if (connection.getKeepAlive() < MAX_TIMEOUT) {

                        sendAndConvert(type, connection);
                        connection.tick();

                }else{
                    on = false;
                    template.convertAndSend("/performance/stop/"+connection.getApiKey(),true);
                }
                } catch (IOException e) {
                    template.convertAndSend("/performance/stop/"+connection.getApiKey(),true);
                    on = false;
                }
            }
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private SimpMessageHeaderAccessor disconnectFrame(){
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.create(SimpMessageType.DISCONNECT);
        accessor.setLeaveMutable(true);
        return accessor;
    }

    private void sendAndConvert(WorkloadType type, SocketConnection connection) throws IOException {
        JmxData data = factory.getJmxData(connection, type);
        String url = constructURL(data.getUrl(), connection);
        Object perfData = data;
        if(data.getData().isPresent()){
            perfData = data.getData().get();
        }
        template.convertAndSend(url,perfData);

    }


    private String constructURL(String urlRoot, SocketConnection connection) {
        return urlRoot + connection.getApiKey();
    }


}
