package com.vua.mpavic.JmxOnline.dao.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ManagedMachine")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ManagedMachine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="managed_machine_id")
    private int id;
    @Column(name="apiKey")
    private String apiKey;
    @Column(name = "jmxUrl")
    private String jmxUrl;
    @Column(name = "name")
    private String name;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = AppUser.class)
    private AppUser user;
    @Column(name = "configuration")
    private String configuration;
    @Column(name="is_on_dashboard")
    private boolean onDashboard;

}
