package com.vua.mpavic.JmxOnline.dao.jmxManagement;

import com.vua.mpavic.JmxOnline.dao.model.Workload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TheWarden implements Runnable {

    Map<String, List<Workload>> workloads;


    public TheWarden(Map<String, List<Workload>> workloads) {
        this.workloads = workloads;
    }

    @Override
    public void run() {
        if(workloads != null && workloads.size() >0){

            workloads.forEach((k,v)->{
                List<Workload> workloadsToRemove = new ArrayList<>();
                v.forEach(workload -> {
                    if(!workload.isOn()){
                        workloadsToRemove.add(workload);
                    }
                });
                v.removeAll(workloadsToRemove);
            });
        }
    }
}
