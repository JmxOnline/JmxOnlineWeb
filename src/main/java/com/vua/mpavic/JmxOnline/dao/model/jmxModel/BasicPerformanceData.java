package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import lombok.Getter;
import lombok.Setter;

import com.sun.management.OperatingSystemMXBean;
import java.lang.management.PlatformManagedObject;
import java.util.Optional;

@Getter
@Setter
public class BasicPerformanceData implements JmxData {

   private String name;
   private String arch;
   private double committedVirtualMemorySize;
   private double freePhysicalMemorySize;
   private double freeSwapSpaceSize;
   private double processCpuLoad;
   private double processCpuTime;
   private double systemCpuLoad;
   private double totalPhysicalMemorySize;
   private double totalSwapSpaceSize;
   private double availableProcessors;

   private final String url = "/performance/simplePerformanceData/";

   public void setPerformanseData(OperatingSystemMXBean performanseData) {
      this.name = performanseData.getName();
      this.arch = performanseData.getArch();
      this.availableProcessors = performanseData.getAvailableProcessors();
      this.committedVirtualMemorySize = performanseData.getCommittedVirtualMemorySize();
      this.freePhysicalMemorySize = performanseData.getFreePhysicalMemorySize();
      this.freeSwapSpaceSize = performanseData.getFreeSwapSpaceSize();
      this.processCpuLoad= performanseData.getProcessCpuLoad();
      this.processCpuTime = performanseData.getProcessCpuTime();
      this.systemCpuLoad = performanseData.getSystemCpuLoad();
      this.totalPhysicalMemorySize = performanseData.getTotalPhysicalMemorySize();
      this.totalSwapSpaceSize = performanseData.getTotalSwapSpaceSize();
   }

   @Override
   public Optional<Object> getData(){
    return  Optional.empty();
   }


   @Override
   public String getUrl() {
      return url;
   }
}
