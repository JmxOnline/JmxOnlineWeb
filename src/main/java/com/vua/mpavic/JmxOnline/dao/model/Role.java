package com.vua.mpavic.JmxOnline.dao.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "role")
@lombok.Getter
@lombok.Setter
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;
    @Column(name = "role_name")
    private String roleName;
    @ManyToMany(mappedBy = "roles")
    List<AppUser> users;
}