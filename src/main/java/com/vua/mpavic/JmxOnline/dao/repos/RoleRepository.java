package com.vua.mpavic.JmxOnline.dao.repos;


import com.vua.mpavic.JmxOnline.dao.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role,Long> {

    Role findRoleByRoleName(String roleName);
}
