package com.vua.mpavic.JmxOnline.dao.repos;

import com.vua.mpavic.JmxOnline.dao.model.ManagedMachine;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;



public interface ManagedMachineRepository extends JpaRepository<ManagedMachine,Integer> {

    public ManagedMachine findByApiKey(String apiKey);

    public List<ManagedMachine> findAllByUser_Username(String username);
    ManagedMachine findManagedMachineById(int id);

    List<ManagedMachine> findAllByUser_UsernameAndOnDashboard(String username,boolean onDashboard);
}
