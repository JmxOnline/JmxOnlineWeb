package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientRegistration {

    private String jmxUrl;
    private String jmxKey;
}
