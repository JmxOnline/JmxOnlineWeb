package com.vua.mpavic.JmxOnline.dao.model;

import com.sun.management.OperatingSystemMXBean;

import com.vua.mpavic.JmxOnline.dao.model.jmxModel.BasicPerformanceData;
import lombok.Getter;
import lombok.Setter;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ManagementFactory;

@Getter
@Setter
public class SocketConnection {
    JmxConnection connection;
    String apiKey;
    int keepAlive;


    public void tick(){
        keepAlive++;
    }

    public void resetCounter(){
        keepAlive = 0;
    }
}
