package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.lang.management.ClassLoadingMXBean;
import java.util.Optional;

@Getter
@Setter
public class ClassLoaderData implements JmxData {
   private double loadedClassCount;
   private double totalLoadedClassCount;
   private double unloadedClassCount;

    private final String url = "/performance/classLoaderData/";

    public void setPerformanseData(ClassLoadingMXBean performanseData) {
        this.loadedClassCount = performanseData.getLoadedClassCount();
        this.totalLoadedClassCount = performanseData.getTotalLoadedClassCount();
        this.unloadedClassCount = performanseData.getUnloadedClassCount();
    }


    @Override
    public Optional<Object> getData() {
        return  Optional.empty();
    }

    @Override
    public String getUrl() {
        return url;
    }
}
