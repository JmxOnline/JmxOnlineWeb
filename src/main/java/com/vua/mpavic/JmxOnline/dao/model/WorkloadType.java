package com.vua.mpavic.JmxOnline.dao.model;

public enum WorkloadType {
    BASIC_PERFORMANCE,
    THREAD_DATA,
    CLASS_LOADER_INFO,
    ALL
}
