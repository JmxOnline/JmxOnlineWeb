package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import com.sun.corba.se.spi.ior.ObjectKey;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import lombok.Getter;
import lombok.Setter;

import java.lang.management.PlatformManagedObject;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class ThreadData implements JmxData {
    private List<SingleThread> threads;
    private final String url = "/performance/threads/";


    public void setThreadData(ThreadMXBean threadData) {
        ThreadInfo[] runAndBlockedThreads = Arrays.stream(threadData.dumpAllThreads(true,true)).
                filter(p -> (p.getThreadState().name().equals("BLOCKED")
                        || p.getThreadState().name().equals("RUNNABLE")))
                .toArray(ThreadInfo[]::new);
        this.threads = new ArrayList<>();

        Arrays.stream(runAndBlockedThreads).forEach(t -> {
            SingleThread thread = new SingleThread(t);
            this.threads.add(thread);
        });
    }


    @Override
    public Optional<Object> getData() {
       return Optional.of(threads);
    }

    @Override
    public String getUrl() {
        return url;
    }
}
