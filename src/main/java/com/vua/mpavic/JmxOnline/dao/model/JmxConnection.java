package com.vua.mpavic.JmxOnline.dao.model;

import lombok.Getter;
import lombok.Setter;

import javax.management.remote.JMXConnector;

@Getter
@Setter
public class JmxConnection {
    private String jmxURl;
    private JMXConnector connector;

}
