package com.vua.mpavic.JmxOnline.dao.model;

public interface Workload extends Runnable {

    void addConnection(SocketConnection connection);
    void setOff();
    void setOn();
    void pause();
    void resume();
    void ping();

    boolean isOn();

    WorkloadType getType();

    String getApiKey();
}
