package com.vua.mpavic.JmxOnline.dao.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
@Getter
@Setter
public class ApiErrorResponce {

    private HttpStatus status;
    private int statusCode;
    private String message;
    private String debugMessage;

    public ApiErrorResponce(HttpStatus status,int statusCode) {
        this.status = status;
        this.statusCode = statusCode;
    }
    public ApiErrorResponce(HttpStatus status,Throwable ex,int statusCode) {
        this.status = status;
        this.statusCode = statusCode;
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiErrorResponce(HttpStatus status,String message,Throwable ex,int statusCode) {
        this.status = status;
        this.statusCode = statusCode;
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }
}
