package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import lombok.Getter;
import lombok.Setter;

import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;

@Getter
@Setter
public class SingleThread {
    private String threadName;
    private long blockedCount;
    private long threadID;
    private String threadState;
    private StackTraceElement[] stackTraceElements;
    private MonitorInfo[] stackTraceLockMonitors;


    public SingleThread(ThreadInfo info) {

        threadName = info.getThreadName();
        blockedCount = info.getBlockedCount();
        threadID = info.getThreadId();
        threadState  = info.getThreadState().name();
        stackTraceElements = info.getStackTrace();
        stackTraceLockMonitors = info.getLockedMonitors();
    }
}
