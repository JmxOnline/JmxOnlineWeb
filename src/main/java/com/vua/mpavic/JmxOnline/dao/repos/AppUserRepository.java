package com.vua.mpavic.JmxOnline.dao.repos;

import com.vua.mpavic.JmxOnline.dao.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser,String> {
     AppUser getByUsername(String username);
}
