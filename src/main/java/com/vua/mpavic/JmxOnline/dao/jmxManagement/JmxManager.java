package com.vua.mpavic.JmxOnline.dao.jmxManagement;


import com.vua.mpavic.JmxOnline.dao.model.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class JmxManager {

    Map<String, List<Workload>> workloads = new HashMap<>();
    ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    TheWarden warden;
    public JmxManager() {
        this.warden = new TheWarden(workloads);

        scheduledExecutorService.scheduleAtFixedRate(warden,0,2,TimeUnit.MINUTES);
    }


    public void addConnection(WorkloadType type,
                              SocketConnection connection,
                              SimpMessagingTemplate template) {
        boolean match = false;
        List<Workload> w = workloads
                .get(connection.getApiKey());
        if (w != null) {
            match = w.stream()
                    .anyMatch(p -> p.getType().equals(type));
        }
        if (!match) {
           addNewWorkload(type,connection,template);
        }else{
           Workload work = w.stream().filter(p -> p.getType().equals(type)).findFirst().get();
           if(!work.isOn()){
               w.remove(work);
               addNewWorkload(type,connection,template);
           }

        }


    }

    private void addNewWorkload(WorkloadType type,
                                SocketConnection connection,
                                SimpMessagingTemplate template) {
        Workload workload =
                createNewWorkload(type, connection, template);
        if (workloads.containsKey(connection.getApiKey())) {
            workloads.get(connection.getApiKey())
                    .add(workload);
        } else {
            List<Workload> tmp = new ArrayList<>();
            tmp.add(workload);
            workloads.put(connection.getApiKey(), tmp);
        }
    }

    public void removeConnection(WorkloadType type, String apikey) {
            List<Workload> workload = workloads.get(apikey);
            if(workload != null){
                this.closeByApiKey(type,workload);


            }
        if(type == WorkloadType.ALL){
            workloads.remove(apikey);
        }



    }

    private void closeByApiKey(WorkloadType type, List<Workload> v) {

        List<Workload> workloadsToRemove = new ArrayList<>();

        v.forEach(mm->{
            if(type != WorkloadType.ALL){
                if(mm.getType() == type){
                    mm.pause();
                    mm.setOff();
                    mm.resume();
                    workloadsToRemove.add(mm);
                }
            }else{
                    mm.pause();
                    mm.setOff();
                    mm.resume();
            }

        });

        workloadsToRemove.forEach(wl ->{
            v.remove(wl);
        });
    }



    public void pingConnection(String apiKey){

        List<Workload> works = workloads.get(apiKey);
        works.forEach((w)-> w.ping());

    }

    private Workload createNewWorkload(WorkloadType type,
                                       SocketConnection connection,
                                       SimpMessagingTemplate template) {
        WorkloadImpl workload = new WorkloadImpl(template);
        workload.setType(type);
        workload.addConnection(connection);
        workload.setOn();
        return workload;

    }
}
