package com.vua.mpavic.JmxOnline.dao.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "user")
@Getter
@Setter

public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUser;

    @Column(unique = true)
    private String username;

    private String password;

    private int enabled;

    private String email;

    @ManyToMany(cascade = {
            CascadeType.ALL
    },fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    @JsonIgnoreProperties(value = {"users"},allowSetters = true)
    private List<Role> roles;
}
