package com.vua.mpavic.JmxOnline.dao.model.jmxModel;

import com.vua.mpavic.JmxOnline.dao.model.SocketConnection;
import com.vua.mpavic.JmxOnline.dao.model.WorkloadType;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;

import com.sun.management.*;

public class JmxDataFactory {

    public JmxData getJmxData(SocketConnection connection, WorkloadType type) throws IOException {
       return getCurrentAppData(connection,type);
    }
    private JmxData getCurrentAppData(SocketConnection connection, WorkloadType type) throws IOException {
        MBeanServerConnection mBeanServerConnection = connection.getConnection().getConnector().getMBeanServerConnection();
        switch (type) {
            case THREAD_DATA: {

                ThreadMXBean threadMXBean = ManagementFactory.newPlatformMXBeanProxy(mBeanServerConnection,
                        ManagementFactory.THREAD_MXBEAN_NAME, com.sun.management.ThreadMXBean.class);
                ThreadData data = new ThreadData();
                data.setThreadData(threadMXBean);
                return data;

            }
            case BASIC_PERFORMANCE: {

                OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.newPlatformMXBeanProxy(mBeanServerConnection,
                        ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME, com.sun.management.OperatingSystemMXBean.class);
                BasicPerformanceData data = new BasicPerformanceData();
                data.setPerformanseData(operatingSystemMXBean);
                return data;
            }
            case CLASS_LOADER_INFO: {
                 ClassLoadingMXBean classLoadingMXBean = ManagementFactory.newPlatformMXBeanProxy(mBeanServerConnection,
                         ManagementFactory.CLASS_LOADING_MXBEAN_NAME, java.lang.management.ClassLoadingMXBean.class);
                 ClassLoaderData data = new ClassLoaderData();
                 data.setPerformanseData(classLoadingMXBean);
                 return data;
             }
            default:{
                throw new IOException();
            }
        }


    }
}
