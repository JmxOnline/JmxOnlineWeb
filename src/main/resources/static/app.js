var stompClient = null;
var apiKey = null;
google.charts.load('current', {'packages':['gauge']});
google.charts.setOnLoadCallback(drawChart);

var chartLoaded =  false;
var chartCPU = null;
var chartMemory = null;

var data;

var optionsCPU = {
    width: 400, height: 120,
    redFrom: 90, redTo: 100,
    yellowFrom:75, yellowTo: 90,
    minorTicks: 5
};


var optionsMemory = {
    width: 400, height: 120,
    redFrom: 90, redTo: 100,
    yellowFrom:75, yellowTo: 90,
    minorTicks: 5
};

function drawChart() {
    chartLoaded = true;
    chartCPU = new google.visualization.Gauge(document.getElementById('performanceDataCpu'));
    chartMemory = new google.visualization.Gauge(document.getElementById('performanceDataMemory'));
    data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['CPU', 0],
        ['Memory', 0]
    ]);

}

function connect() {
    var socket = new SockJS('/performance-MM');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {

        stompClient.subscribe('/performance/performanceData', function (message) {
            showGreeting(message);

        });
        repeatingCalls();
        window.setInterval(function () {
            repeatingCalls()
        },1000)
    });
}
function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}
function showGreeting(message) {
    console.log("This is message" + JSON.stringify(message));
    var body = JSON.parse(message.body)

    if(chartLoaded){
        if(chartCPU != null){
            data.setValue(0,1, body.systemCpuLoad * 100);
            optionsMemory.redFrom = (body.totalPhysicalMemorySize/1024)/1024 - 2000;
            optionsMemory.redTo = (body.totalPhysicalMemorySize/1024)/1024;
            data.setValue(1,1,(body.freePhysicalMemorySize/1024)/1024)

            chartCPU.draw(data, optionsCPU);
            chartMemory.draw(data,optionsMemory)
        }
    }



}

function repeatingCalls(){
    stompClient.send("/app/performance", {}, JSON.stringify({message:apiKey}));
}

$(function () {

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'http://localhost:8080/jmx/connections',
        dataType: 'json',
        contentType:'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify(1),
        success: function (response) {
            apiKey = response;
            connect();
        },
        error: function (er) {

            apiKey = er.responseText;
            connect();
        }
    })


});